﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    //cria variavel publica para controlar a velocidade do movimento via UnityEditor
    public float speed;      
    
    //cria variavel do corpo rigido para manter como referencia
    private Rigidbody rb;    

    //inicia funcao do corpo rigido
    void Start () {
        rb = GetComponent<Rigidbody>();
	}

    //funcao para mover corpo rigido usando 3 vetores (mover horizontalmente, estatico, mover verticalmente)
    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        //determina direcao e movimento ao objeto usando Vector3 (x, y, z)
        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        //controla movimento Vs velocidade
        rb.AddForce(movement * speed);        
    }
}